package org.samples.orderdto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountValue {
  long amount;

  public boolean reduceAmount(int reduceBy) {
    if (amount < reduceBy) {
      return false;
    } else {
      amount = amount - reduceBy;
      return true;
    }
  }

  public void addAmount(long addBy) {
    this.amount = this.amount + addBy;
  }
}
