package org.samples.orderdto;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CompensationValue {
  boolean isFailed = false;
  List<CompensationDTO> compensations = new ArrayList<>();

  public void addCompensation(CompensationDTO compensation) {
    this.compensations.add(compensation);
    if (compensation.isFailed) {
      this.isFailed = true;
    }
  }

  public void emptyCompensations() {
    this.compensations = new ArrayList<>();
  }
}
