package org.samples.orderdto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ItemValue {
  long count;

  public boolean reduceCount(int reduceBy) {
    if (count - reduceBy < 0) {
      return false;
    }
    count = count - reduceBy;
    return true;
  }

  public void addQuantity(long addBy) {
    this.count = this.count + addBy;
  }
}
