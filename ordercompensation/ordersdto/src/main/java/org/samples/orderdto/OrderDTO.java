package org.samples.orderdto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDTO {
  String orderId;
  String accountId;
  String itemId;
  int amount;
  int quantity;
}
