package org.samples.orderdto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompensationDTO {
  String app;
  String action;
  String compensationAction;
  String compensationTable;
  String compensationKey;
  boolean isFailed;
  String orderId;
}
