package org.samples.orderdto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderValue {
  String status;

  public void setStatus(String status) {
    this.status = status;
  }
}
