# Microservice example with compensating transactions

## Prerequisites
- Grainite running either as a trial instance or local docker container
- Grainite client & its prerequistes https://gitlab.com/grainite/samples/-/blob/main/quickstart.md


## Setup
1. Clone this repo to your machine and cd to ordercompensation directory
    ```
    git clone https://gitlab.com/grainite/blogs.git
    cd ordercompensation
    ```

2. Build the applications and download necessary dependencies
    ```
    mvn compile package
    ```

3. You are now ready to connect to the Grainite instance, load and run the 3 apps. 
   If you are running the local docker instance, you do not pass any params
   If you are connecting to the Grainite trial instance, pass in the IP address
   of the trial instance that the Grainite team provided you as well as the location 
   of the grainite samples directory since we will pick up the certs & key from there.
   If using local docker instance:
    ```
    cd account_service; gx load; cd ..
    cd inventory_service; gx load; cd ..
    cd order_service; gx load; cd ..
    ./send_order_event.sh
    ```
    If using Grainite trial instance:
    ```
    cd account_service; gx load -H {SERVER_IP}; cd ..
    cd inventory_service; gx load -H {SERVER_IP}; cd ..
    cd order_service; gx load -H {SERVER_IP}; cd ..
    ./send_order_event.sh {SERVER_IP} 
    ```

