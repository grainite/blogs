package org.samples.orderservice;

import com.grainite.api.Event;
import com.grainite.api.Grain;
import com.grainite.api.Grainite;
import com.grainite.api.GrainiteClient;
import com.grainite.api.Table;
import com.grainite.api.Topic;
import com.grainite.api.Value;
import com.jsoniter.output.JsonStream;
import java.io.File;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Random;
import org.samples.orderdto.AccountValue;
import org.samples.orderdto.ItemValue;
import org.samples.orderdto.OrderDTO;

public class Client {
  static Random random = new Random();
  static Grainite client;
  public static void main(String[] args) throws InterruptedException {
    System.out.println("args: " + Arrays.toString(args));
    if (args.length == 0) {
      // Assume we are using the local docker image
      System.out.println("Connecting to grainite on localhost");
      client = GrainiteClient.getClient("localhost", 5056);
    } else if (args.length == 2 || args.length == 3) {
      // Connecting to a remote, secure Grainite instance
      File caCert = Paths.get(args[0]).toFile();
      File clientCert = Paths.get(args[1]).toFile();
      File clientKey = Paths.get(args[2]).toFile();
      String host = args[3];
      String envName = args.length == 5 ? args[4] : "default";
      System.out.println("Connecting to grainite on: " + host);
      if (caCert.exists() && clientCert.exists() && clientKey.exists()) {
        client = GrainiteClient.getClient(host, 5056, caCert.getAbsolutePath(),
            clientCert.getAbsolutePath(), clientKey.getAbsolutePath(), envName);
      } else {
        client = GrainiteClient.getClient(host, 5056, envName);
      }
    }

    // Get topic order_events_topic.
    long ts = System.currentTimeMillis();

    System.out.println("Creating 10 items");
    Table itemTable = client.getTable(Constants.INVENTORY_SERVICE, Constants.ITEM_TABLE);
    for (int i = 0; i < 10; i++) {
      Grain item = itemTable.getGrain(Value.of("item-" + i));
      item.setValue(Value.of(JsonStream.serialize(new ItemValue(random.nextInt(5) + 5))));
    }
    System.out.println("Creating 10 accounts");
    Table accountTable = client.getTable(Constants.ACCOUNT_SERVICE, Constants.ACCOUNT_TABLE);
    for (int i = 0; i < 10; i++) {
      Grain account = accountTable.getGrain(Value.of("account-" + i));
      account.setValue(Value.of(JsonStream.serialize(new AccountValue(random.nextInt(300) + 200))));
    }

    System.out.println("sending 10 orders");
    Topic topic = client.getTopic(Constants.APP_NAME, Constants.ORDER_EVENTS_TOPIC);
    for (int i = 0; i < 10; i++) {
      String key = "order-" + ts + "-" + i;
      OrderDTO order = new OrderDTO(key, "account-" + random.nextInt(10),
          "item-" + random.nextInt(10), random.nextInt(200) + 50, random.nextInt(5) + 2);

      Event topicEvent = new Event(Value.of(key), Value.of(JsonStream.serialize(order)));
      // Append event to topic.
      topic.append(topicEvent);
    }
    System.out.println("Sent orders");
    Thread.sleep(2000);
    System.out.println("Checking status of order history");

    Table orderHistoryTable = client.getTable(Constants.APP_NAME, Constants.COMPENSATION_TABLE);
    for (int i = 0; i < 10; i++) {
      Grain orderHistory = orderHistoryTable.getGrain(Value.of("order-" + ts + "-" + i));
      System.out.println("Order: K> " + orderHistory.getKey().asString());
      System.out.println("Order: V> " + orderHistory.getValue().asString());
      System.out.println("----------------------------");
    }

    System.out.println("----------------------------");
    System.out.println("Printing status of each order");
    Table orderTable = client.getTable(Constants.APP_NAME, Constants.ORDER_TABLE);
    for (int i = 0; i < 10; i++) {
      Grain order = orderTable.getGrain(Value.of("order-" + ts + "-" + i));
      System.out.println("Order: K> " + order.getKey().asString());
      System.out.println("Order: V> " + order.getValue().asString());
      System.out.println("----------------------------");
    }
    client.close();
  }
}
