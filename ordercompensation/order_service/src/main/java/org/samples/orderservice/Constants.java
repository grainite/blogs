package org.samples.orderservice;

/**
 * This class was generated by GX.
 */
public class Constants {
  public static String APP_NAME = "orderservice";

  public static String ORDER_EVENTS_TOPIC = "order_events_topic";

  public static String ORDER_TABLE = "order_table";

  public static String HANDLE_INCOMING_ORDER_EVENT_ACTION = "handleIncomingOrderEvent";

  public static String ORDEREVENTS_SUBSCRIPTION = "orderevents";

  public static String PROCESS_ORDER_ACTION = "processOrder";

  public static String COMPENSATE_PROCESS_ORDER_ACTION = "compensateProcessOrder";

  public static String COMPENSATION_TABLE = "compensation_table";

  public static String REGISTER_COMPENSATION_ACTION = "registerCompensation";

  public static String TRIGGER_COMPENSATION_ACTIONS = "triggerCompensations";

  public static String INVENTORY_SERVICE = "inventoryservice";

  public static String ITEM_TABLE = "item_table";

  public static String EXECUTE_ITEM_REQUEST_ACTION = "executeItemRequest";

  public static String ACCOUNT_SERVICE = "accountservice";

  public static String ACCOUNT_TABLE = "account_table";

  public static String CHECK_ACCOUNT_ACTION = "checkAccount";

  public static String UPDATE_ORDER_STATUS_ACTION = "updateOrderStatus";

  public static String PENDING = "pending";
  public static String SUCCESS = "success";
  public static String FAILED = "failed";
}
