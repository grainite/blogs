#!/bin/bash


if [ $# -gt 2 ] 
then
    echo ====== $#
    echo 'Usage: < host IP > < samples dir > '
    exit 1
fi

if [ $# -eq 2 ] 
then
    HOST=$1
    SAMPLES_DIR=$2
    CA_CERT="${SAMPLES_DIR}/certs/grainite_ca.crt"
    CLIENT_CERT="${SAMPLES_DIR}/certs/grainite_client.test-client.crt"
    CLIENT_KEY="${SAMPLES_DIR}/certs/grainite_client.test-client.key"
    echo java -cp order_service/target/order_service-jar-with-dependencies.jar org.samples.orderservice.Client $CA_CERT $CLIENT_CERT $CLIENT_KEY $HOST
    java -cp order_service/target/order_service-jar-with-dependencies.jar org.samples.orderservice.Client $CA_CERT $CLIENT_CERT $CLIENT_KEY $HOST
else
    echo ava -cp order_service/target/order_service-jar-with-dependencies.jar org.samples.orderservice.Client $*
    java -cp order_service/target/order_service-jar-with-dependencies.jar org.samples.orderservice.Client $*
fi